﻿using Domain.Common;

namespace Domain.Entities.Identity
{
    public class Role : BaseEntity
    {
        public string Code { get; set; }
        public ICollection<User> Users { get; set;}

        public Role(string code)
        {
            Code = code;
        }
    }
}
