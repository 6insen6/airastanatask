﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Enums
{
    public enum Status
    {
        InTime = 0,
        Delayed = 1,
        Cancelled = 2
    }
}
