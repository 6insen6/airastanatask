﻿using Application.Flights.Commands;
using Application.Flights.Queries;
using Domain.Entities;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Web.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class FlightsController : ControllerBase
    {
        private readonly IMediator _mediator;

        public FlightsController(IMediator mediator)
        {
            _mediator = mediator;
        }

        /// <summary>
        /// Добавление нового рейса
        /// </summary>
        /// <param name="command"></param>
        /// <returns></returns>
        [HttpPost]
        [Authorize(Roles = "Moderator")]
        public async Task Create(CreateFlightCommand command)
        {
            await _mediator.Send(command);
        }

        /// <summary>
        /// Редактирование рейса
        /// </summary>
        /// <param name="command"></param>
        /// <returns></returns>
        [HttpPut]
        [Authorize(Roles = "Moderator")]
        public async Task Update(UpdateFlightCommand command)
        {
            await _mediator.Send(command);
        }

        /// <summary>
        /// Получение списка рейсов
        /// </summary>
        /// <param name="command"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<List<Flight>> List([FromQuery] GetFlightsQuery command)
        {
            return await _mediator.Send(command);
        }
    }
}
