﻿using Microsoft.IdentityModel.Tokens;
using System.Text;

namespace Web
{
    public class AuthOptions
    {
        public const string ISSUER = "FlightStatusServer";
        public const string AUDIENCE = "FlightStatusClient";
        const string KEY = "94o4q627pvcc1axq94o4q627pvcc1axq"; 
        public const int LIFETIME = 120;
        public static SymmetricSecurityKey GetSymmetricSecurityKey()
        {
            return new SymmetricSecurityKey(Encoding.ASCII.GetBytes(KEY));
        }
    }
}
