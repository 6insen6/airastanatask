﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Web.Middleware
{
    public class ErrorResponse
    {
        public string Message { get; set; }
        public bool Success { get; } = false;
    }
}
