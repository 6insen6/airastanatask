﻿using Application.Common.Interfaces;
using Domain.Entities;
using Infrastructure.Data.Helpers;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Caching.Memory;

namespace Application.Flights.Queries
{
    public record GetFlightsQuery: IRequest<List<Flight>>
    {
        public string? Origin { get; init; }
        public string? Destination { get; init; }
    }

    public class GetFlightsQueryHandler : IRequestHandler<GetFlightsQuery, List<Flight>>
    {
        private readonly IApplicationDbContext _context;
        private readonly IMemoryCache _cache;

        public GetFlightsQueryHandler(IApplicationDbContext context,
            IMemoryCache cache)
        {
            _context = context;
            _cache = cache;
        }

        public async Task<List<Flight>> Handle(GetFlightsQuery request, CancellationToken cancellationToken)
        {
            string cacheKey = $"{request.Origin}-{request.Destination}";

            if (_cache.TryGetValue(cacheKey, out List<Flight> cachedFlights))
            {            
                return cachedFlights;
            }

            var query = _context.Flights.AsQueryable();

            if (!String.IsNullOrEmpty(request.Destination))
            {
                query = query.Where(x => x.Destination.ToUpper() == request.Destination.ToUpper());
            }

            if (!String.IsNullOrEmpty(request.Origin))
            {
                query = query.Where(x => x.Origin.ToUpper() == request.Origin.ToUpper());
            }

            var flights = await query.ToListAsync();

            if (flights.Any() && cacheKey != "-")
            {
                _cache.Set(cacheKey, flights);
                _cache.AppendKeyIfNotExists(cacheKey);
            }

            return flights;
        }
    }
}
