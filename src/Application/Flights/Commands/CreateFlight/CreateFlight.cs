﻿using Application.Common.Helpers;
using Application.Common.Interfaces;
using Domain.Entities;
using Domain.Enums;
using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore.Metadata.Internal;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Logging;

namespace Application.Flights.Commands
{
    public record CreateFlightCommand : IRequest<int> {
        public string Origin { get; init; }
        public string Destination { get; set; }
        public DateTimeOffset Departure { get; set; }
        public DateTimeOffset Arrival { get; set; }
    }

    public class CreateFlightCommandHandler : IRequestHandler<CreateFlightCommand, int>
    {
        private readonly IApplicationDbContext _context;
        private readonly ILogger<CreateFlightCommandHandler> _logger;
        private readonly IMemoryCache _cache;
        private int _userId;
        public CreateFlightCommandHandler(IApplicationDbContext context,
            ILogger<CreateFlightCommandHandler> logger,
            IMemoryCache cache,
            IHttpContextAccessor accessor)
        {
            _context = context;
            _logger = logger;
            _cache = cache;
            _userId = UserHelper.GetUserIdFromContext(accessor.HttpContext);
        }

        public async Task<int> Handle(CreateFlightCommand request, CancellationToken cancellationToken)
        {
            var entity = new Flight
            {
                Arrival = request.Arrival,
                Destination = request.Destination,
                Departure = request.Departure,
                Origin = request.Origin,
                Status = Status.InTime
            };

            await _context.Flights.AddAsync(entity);
            await _context.SaveChangesAsync(cancellationToken);

            RefreshCache(entity);

            _logger.LogInformation($"Create flight {entity.Id} by userId:{_userId}");
            return entity.Id;
        }

        private void RefreshCache(Flight entity)
        {
            var cacheKeys = _cache.Get<HashSet<string>>("keys");

            if (cacheKeys == null)
                return;

            foreach (var key in cacheKeys)
            {
                var splittedKey = key.Split('-');
                //Алматы-Астана
                if ((key.Contains(entity.Origin) && key.Contains(entity.Destination))
                    //Алматы-
                    || (String.IsNullOrEmpty(splittedKey[1]) && splittedKey[0].Contains(entity.Origin))
                    //-Алматы
                    || (String.IsNullOrEmpty(splittedKey[0]) && splittedKey[1].Contains(entity.Destination))
                    )
                {
                    AppendCache(key, entity);
                }
            }
        }

        private void AppendCache(string key, Flight entity)
        {
            var flights = _cache.Get<List<Flight>>(key);
            flights.Add(entity);
            _cache.Set(key, flights);
        }
    }
}
