﻿using FluentValidation;
using MediatR;

namespace Application.Flights.Commands
{
    public class CreateFlightCommandValidator : AbstractValidator<CreateFlightCommand>
    {
        public CreateFlightCommandValidator() {
            RuleFor(v => v.Origin)
                .MaximumLength(256)
                .NotEmpty();

            RuleFor(v => v.Destination)
                .MaximumLength(256)
                .NotEmpty();

            RuleFor(v => v.Arrival)
                .GreaterThan(v => v.Departure)
                .WithMessage("Значение Arrival должно быть больше значения Departure");
        }
    }
}
