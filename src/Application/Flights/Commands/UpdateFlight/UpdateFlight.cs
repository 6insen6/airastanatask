﻿using Application.Common.Helpers;
using Application.Common.Interfaces;
using Domain.Entities;
using Domain.Enums;
using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Logging;
using System.Security.Claims;

namespace Application.Flights.Commands
{
    public record UpdateFlightCommand : IRequest
    {
        public int Id { get; set; }
        public Status Status { get; set; }
    }

    public class UpdateFlightCommandHandler : IRequestHandler<UpdateFlightCommand>
    {
        private readonly IApplicationDbContext _context;
        private readonly ILogger<UpdateFlightCommandHandler> _logger;
        private readonly IMemoryCache _cache;
        private int _userId;

        public UpdateFlightCommandHandler(IApplicationDbContext context,
            ILogger<UpdateFlightCommandHandler> logger,
            IMemoryCache cache,
            IHttpContextAccessor accessor)
        {
            _context = context;
            _logger = logger;
            _cache = cache;

            _userId = UserHelper.GetUserIdFromContext(accessor.HttpContext);
        }

        public async Task Handle(UpdateFlightCommand request, CancellationToken cancellationToken)
        {
            var entity = await _context.Flights.FindAsync(new object[] { request.Id }, cancellationToken);

            if (entity == null)
            {
                throw new Exception($"Рейс {request.Id} не найден");
            }

            entity.Status = request.Status;
            await _context.SaveChangesAsync(cancellationToken);

            RefreshCache(entity);

            _logger.LogInformation($"Update flight {entity.Id} by userId:{_userId}");
        }

        private void RefreshCache(Flight entity)
        {
            //Получаем все ключи поиска
            var cacheKeys = _cache.Get<HashSet<string>>("keys");
            if (cacheKeys == null)
                return;
            foreach (var key in cacheKeys)
            {
                var splittedKey = key.Split('-');
                //Алматы-Астана
                if ((key.Contains(entity.Origin) && key.Contains(entity.Destination)) 
                    //Алматы-
                    || (String.IsNullOrEmpty(splittedKey[1]) && splittedKey[0].Contains(entity.Origin))
                    //-Алматы
                    || (String.IsNullOrEmpty(splittedKey[0]) && splittedKey[1].Contains(entity.Destination))
                    )
                {
                    UpdateCache(key, entity);
                }
            }
        }

        private void UpdateCache(string key, Flight entity)
        {
            var flights = _cache.Get<List<Flight>>(key);
            var flight = flights.FirstOrDefault(x => x.Id == entity.Id);

            flight.Status = entity.Status;

            _cache.Set(key, flights);
        }
    }

}
