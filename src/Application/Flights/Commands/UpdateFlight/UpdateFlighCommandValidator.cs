﻿using Domain.Enums;
using FluentValidation;

namespace Application.Flights.Commands.UpdateFlight
{
    public class UpdateFlighCommandValidator : AbstractValidator<UpdateFlightCommand>
    {
        public UpdateFlighCommandValidator()
        {
            RuleFor(v => v.Status)
                .IsInEnum()
                .WithMessage("Значение должно быть в диапазоне enum");
        }
    }
}
