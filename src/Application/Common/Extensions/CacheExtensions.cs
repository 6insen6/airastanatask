﻿using Microsoft.Extensions.Caching.Memory;

namespace Infrastructure.Data.Helpers
{
    public static class CacheExtensions
    {
        public static IMemoryCache AppendKeyIfNotExists(this IMemoryCache cache, string cacheKey)
        {
            if (cache.TryGetValue("keys", out HashSet<string> cacheKeys))
            {
                cacheKeys.Append(cacheKey);
                cache.Set("keys", cacheKeys);
            }
            else
            {
                cache.Set("keys", new HashSet<string>() {
                    cacheKey
                });
            }
            return cache;
        }
    }
}
