﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace Application.Common.Helpers
{
    public static class UserHelper
    {
        public static int GetUserIdFromContext(HttpContext context)
        {
            int userId = 0;
            if (context != null)
            {
                var id = context.User.FindFirst(ClaimTypes.NameIdentifier)?.Value;
                int.TryParse(id, out userId);
            }
            return userId;
        }
    }
}
