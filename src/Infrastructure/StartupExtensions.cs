﻿using Application.Common.Interfaces;
using FlightStatus.Infrastructure.Data;
using Infrastructure.Data;
using Infrastructure.Data.Interceptors;
using Infrastructure.Data.UserManager;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Diagnostics;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace Infrastructure
{
    public static class StartupExtensions
    {
        public static IServiceCollection AddInfrastructureServices(this IServiceCollection services, IConfiguration configuration)
        {
            var connectionString = configuration.GetConnectionString("DefaultConnection");

            services.AddScoped<ISaveChangesInterceptor, AuditableEntityInterceptor>();
            services.AddDbContext<IApplicationDbContext, ApplicationDbContext>((sp, options) =>
            {
                options.UseSqlServer(connectionString);
                options.AddInterceptors(sp.GetServices<ISaveChangesInterceptor>());
            });
            services.AddScoped<ApplicationDbContextInitialiser>();
            services.AddScoped<IUserManager, UserManager>();
            return services;
        }
    }
}
