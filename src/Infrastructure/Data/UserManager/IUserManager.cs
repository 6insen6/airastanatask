﻿using Domain.Entities.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure.Data.UserManager
{
    public interface IUserManager
    {
        Task<User> GetUserByNameAsync(string userName);
        Task<bool> VerifyPasswordAsync(User user, string password);
    }
}
