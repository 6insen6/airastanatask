﻿using Domain.Entities.Identity;
using Microsoft.EntityFrameworkCore;
using System;
using System.Buffers.Text;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure.Data.UserManager
{
    public class UserManager : IUserManager
    {
        private readonly ApplicationDbContext _context;

        public UserManager(ApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<User> GetUserByNameAsync(string userName)
        {
            var user = await _context.Users
                .Include(x => x.Role)
                .FirstOrDefaultAsync(x => x.UserName.ToUpper() == userName.ToUpper());

            return user;
        }

        public async Task<bool> VerifyPasswordAsync(User user, string password)
        {
            if (user.Password != PasswordHasher.CreateMD5(password)) {
                return false;
            }
            return true;
        }
    }
}
