﻿using Domain.Entities.Identity;
using Infrastructure.Data;
using Infrastructure.Data.UserManager;
using Microsoft.AspNetCore.Builder;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;

namespace FlightStatus.Infrastructure.Data
{
    public static class InitialiserExtensions
    {
        public static async Task InitialiseDatabaseAsync(this WebApplication app)
        {
            using var scope = app.Services.CreateScope();

            var initialiser = scope.ServiceProvider.GetRequiredService<ApplicationDbContextInitialiser>();

            await initialiser.InitialiseAsync();
            await initialiser.SeedAsync();
        }
    }
    public class ApplicationDbContextInitialiser
    {
        private ApplicationDbContext _context;

        public ApplicationDbContextInitialiser(ApplicationDbContext context)
        {
            _context = context;
        }

        public async Task InitialiseAsync()
        {
            try
            {
                await _context.Database.MigrateAsync();
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public async Task SeedAsync()
        {
            try
            {
                await TrySeedAsync();
            }
            catch (Exception ex)
            {
                //_logger.LogError(ex, "An error occurred while seeding the database.");
                throw;
            }
        }

        private async Task TrySeedAsync()
        {
            // Default roles
            List<Role> roles = new List<Role>()
            {
                new Role("Client"),
                new Role("Moderator"),
            };

            // Default users
            List<User> users = new List<User>()
            {
                new User()
                {
                    Role = roles[0],
                    Password = PasswordHasher.CreateMD5("123456"),
                    UserName = "client"
                },
                new User()
                {
                    Role = roles[1],
                    Password = PasswordHasher.CreateMD5("123123"),
                    UserName = "moderator"
                },
            };

            // Default data
            // Seed, if necessary
            if (!_context.Roles.Any())
            {
                await _context.Roles.AddRangeAsync(roles);
                await _context.SaveChangesAsync();
            }

            if (!_context.Users.Any())
            {
                await _context.Users.AddRangeAsync(users);
                await _context.SaveChangesAsync();
            }
        }
    }
}
