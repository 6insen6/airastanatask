# Задача на получение данных о статусах рейсов

Перед запуском в секретах (либо в appsettings) нужно указать строку подключения. Миграции и сидирование применятся при старте приложения WEB


```
"ConnectionStrings": {
    "DefaultConnection": "Server={};Database=FlightTracker;User Id={};Password={};TrustServerCertificate=True;"
  }
```

**Пользователи**

Клиент
> client:123456

Модератор
> moderator:123123

Хэширование пароля использовал MD5, просто в качестве примера.

Сильного много тесты расписывать не стал, протестил валидацию в CreateCommand и запись в UpdateCommand.