﻿using Application.Common.Interfaces;
using Application.Flights.Commands;
using Domain.Entities;
using Domain.Enums;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Logging;
using Moq;

namespace UnitTests
{
    public class UpdateFlightHandlerTests
    {
        [Fact]
        public async Task Handle_UpdatesFlightStatusAndLogsInformation()
        {
            // Arrange
            var entityId = 123;
            var entity = new Flight { Id = entityId, Status = Status.InTime };
            var request = new UpdateFlightCommand { Id = entityId, Status = Status.Delayed };

            var mockContext = new Mock<IApplicationDbContext>();
            mockContext.Setup(c => c.Flights.FindAsync(It.IsAny<object[]>(), It.IsAny<CancellationToken>()))
                       .ReturnsAsync(entity);

            var mockLogger = new Mock<ILogger<UpdateFlightCommandHandler>>();

            var mockCache = new Mock<IMemoryCache>();

            var accessor = new Mock<IHttpContextAccessor>();

            var handler = new UpdateFlightCommandHandler(mockContext.Object, mockLogger.Object, mockCache.Object, accessor.Object);

            // Act
            await handler.Handle(request, CancellationToken.None);

            // Assert
            Assert.Equal(Status.Delayed, entity.Status);
            mockContext.Verify(c => c.SaveChangesAsync(CancellationToken.None), Times.Once);
        }
    }
}
