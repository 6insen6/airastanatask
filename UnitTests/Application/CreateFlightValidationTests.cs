using Application.Flights.Commands;
using FluentValidation.TestHelper;

namespace UnitTests
{
    public class CreateFlightValidationTests
    {
        private readonly CreateFlightCommandValidator _validator;

        public CreateFlightValidationTests()
        {
            _validator = new CreateFlightCommandValidator();
        }

        [Theory]
        [InlineData(null)]
        [InlineData("")]
        [InlineData(" ")]
        public void OriginNullOrEmptyTests(string origin)
        {
            // Arrange
            var command = new CreateFlightCommand { Origin = origin };

            // Act
            var result = _validator.TestValidate(command);

            // Assert
            result.ShouldHaveValidationErrorFor(x => x.Origin);
        }

        [Fact]
        public void OriginExceedMaximumLength()
        {
            // Arrange
            var origin = new string('A', 257);
            var command = new CreateFlightCommand { Origin = origin };

            // Act
            var result = _validator.TestValidate(command);

            // Assert
            result.ShouldHaveValidationErrorFor(x => x.Origin);
        }
    }
}